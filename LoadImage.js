import {loadImage, panel_right, ctx, canvas} from "./DOM.js";
import {history} from "./util.js";



const LoadImage = {

  handleFile: function(e){

    // ************* start image loading *************
    // 
    // document fragments (created by document.importNode()) do not have remove() method
    // so we can't use show.remove() to remove previous image preview, just requery it
    // for better using experience, we remove preview image preview only after the new image is read onto <img>
    let prevShow = document.querySelector(".load-image");

    let show = document.importNode(loadImage.template.content, true);
    let imageElement = show.querySelector("img");
    let adjustElements = show.querySelectorAll("input");
    let widthElement = adjustElements[0];
    let heightElement = adjustElements[1];
    let proportionalElement = adjustElements[2];
    imageElement.file = this.files[0];
    // console.log(this.files)

    const reader = new FileReader();
    // reader.onload = (function(html_img){
    //   return (e) => {
    //     html_img.src = e.target.result;
    //     console.log(e);
    //   }
    // })(imageElement);
    reader.onload = (e) => {
      imageElement.src = e.target.result;
      if(prevShow) prevShow.remove();
      console.log(e);
    };
    reader.readAsDataURL(this.files[0]);

    imageElement.onload = () => {
      // 調整圖片尺寸
      // there is a fucking fucking fucking pitfall
      // from the beginning, if you always modify it's width (or always height), it AUTOMATICALLY resize the image proportionally 
      // but after you do the first modification of height after at least 1 width modification (or first modification of width after height), all the later modifications will finally do what you expect (i.e.not proportionally)
      // and this pitfall seems to happen only in the <img> in which src is loaded by FileReader in this way
      // if(window.innerWidth < imageElement.width){
      //   adjustSize("width", window.innerWidth - 200, true);
      // }
      // show size
      widthElement.value = imageElement.width;
      heightElement.value = imageElement.height;
    };
    // 
    // ************* end image loading *************


    // ************* start image adjusting *************
    // 
    // adjustOption: "width" or "height"
    function adjustSize(adjustOption, value, proportional){
      console.log("proportional = ", proportional);
      if(proportional){
        // here will not behavior as you want because of the pitfall mentioned above
        // i.e. height will be factored twice
        let factor = value / imageElement[adjustOption];
        // console.log(imageElement.width);
        // console.log(imageElement.height);
        imageElement.width *= factor;
        // console.log(imageElement.width);
        // console.log(imageElement.height);
        imageElement.height *= factor;
        // console.log(imageElement.width);
        // console.log(imageElement.height);
      } else{
        imageElement[adjustOption] = value;
      }
      // avoid width&height to be 0
      // I'll not do this revision because it may fix(for user) and hide(for developer) the pitfall mentioned above
    }
    function customSize(e){
      if(this.value === "") return;
      let newDimension = parseInt(this.value);
      adjustSize(this.dataset.adjust_option, newDimension, proportionalElement.checked);
      updateInfo();
    }
    function updateInfo(){
      widthElement.value = imageElement.width;
      heightElement.value = imageElement.height;
    }
    widthElement.addEventListener("keyup", customSize.bind(widthElement));
    heightElement.addEventListener("keyup", customSize.bind(heightElement));
    // 
    // ************* end image adjusting *************


    // ************* start image moving *************
    // 
    let clickStage = 0;
    let cursorOffsetLeft;
    let cursorOffsetTop;
    function moveImage(e){
      imageElement.style.left = e.pageX - cursorOffsetLeft;
      imageElement.style.top = e.pageY - cursorOffsetTop;
    }
    imageElement.addEventListener("click", (e) => {
      if(clickStage === 0){
        clickStage = 1;
        imageElement.style.cursor = "move";
        cursorOffsetTop = e.pageY - imageElement.offsetTop;
        cursorOffsetLeft = e.pageX - imageElement.offsetLeft;
        // this "position: absolute;" should be executed after accessing imageElement.offsetLeft / offsetTop
        // because it may change imageElement's position
        // position: absolute; will make imageElement out of the layout flow of it's parent element
        // , which may change the parent element's width(if imageElement is the widest element in the parent element)
        // if it's parent element is in the flexbox, the parent element will be re-aligned to center
        // , and consequently change this element's base point
        imageElement.style.position = "absolute";
        document.addEventListener("mousemove", moveImage);
      }
      else if (clickStage === 1){
        // https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Using_images#Controlling_image_scaling_behavior
        // https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/drawImage
        ctx.drawImage(
          imageElement, 
          parseInt(imageElement.style.left) - canvas.offsetLeft, 
          parseInt(imageElement.style.top) - canvas.offsetTop, 
          imageElement.width, 
          imageElement.height
          );
        document.removeEventListener("mousemove", moveImage);
        document.querySelector(".load-image").remove();
        history.susume();
        // reset input
        this.value = "";
      }
    });
    // 
    // ************* end image moving *************

    panel_right.appendChild(show);
  },

  eventListenerRegister: function(){
    loadImage.hidden.addEventListener("change", this.handleFile);
    loadImage.btn.addEventListener("click", () => {
      loadImage.hidden.click();
    });
  }
};


export {LoadImage};