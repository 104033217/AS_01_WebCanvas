import {eraser} from "./DOM.js";
import {util} from "./util.js";

const Eraser = {

  eventListenerRegister: function(){
    eraser.btn.addEventListener("click", util.changeBrush.bind(null, "eraser"))
  }
}

export {Eraser};