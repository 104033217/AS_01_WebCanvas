import {brush, ctx} from "./DOM.js";
import {status, util} from "./util.js";

const Brush = {
  draw: function (e){
    let [x, y] = [e.offsetX, e.offsetY];
    ctx.beginPath();
    ctx.moveTo(status.lastX, status.lastY);
    ctx.lineTo(x, y);
    ctx.stroke();
    status.lastX = x;
    status.lastY = y;
  },

  eventListenerRegister: function(){
    brush.btn.addEventListener("click", util.changeBrush.bind(null, "brush"));
  }
}

export {Brush};