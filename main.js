import {ColorPicker as CP} from "./ColorPicker.js";
import {ResetCanvas} from "./ResetCanvas.js";
import {StrokeWidth} from "./StrokeWidth.js";
import {Undo} from "./Undo.js";
import {Brush} from "./Brush.js";
import {LoadImage} from "./LoadImage.js";
import {Rectangle} from "./Rectangle.js";
import {Triangle} from "./Triangle.js";
import {Eraser} from "./Eraser.js";
import {SaveImage} from "./SaveImage.js";
import {TextInput} from "./TextInput.js";
import {canvas, ctx, drawCursor} from "./DOM.js";
import {util, init, status, config, history} from "./util.js";


CP.init();
CP.eventListenerRegister();
StrokeWidth.eventListenerRegister();
StrokeWidth.updateCursorSvg();
ResetCanvas.eventListenerRegister();
Undo.eventListenerRegister();
Brush.eventListenerRegister();
LoadImage.eventListenerRegister();
Rectangle.eventListenerRegister();
Triangle.eventListenerRegister();
Eraser.eventListenerRegister();
SaveImage.eventListenerRegister();
TextInput.eventListenerRegister();

// initialize
init.canvasWidth = screen.availWidth * 0.5;
init.canvasHeight = screen.availHeight * 0.7;
canvas.width = init.canvasWidth;
canvas.height = init.canvasHeight;
// console.log(ctx);
// console.log(ctx.getImageData(0, 0, init.canvasWidth, init.canvasHeight));
ctx.lineJoin = "round";
ctx.lineCap = "round";


// main.js
// below is the settings of event listeners on document / main canvas

document.addEventListener("mouseup", (e) => {
  if(status.isDrawing){
    status.isDrawing = false;
    history.susume();
  }
  if(status.colorPicking1 || status.colorPicking2){
    status.colorPicking1 = false;
    status.colorPicking2 = false;
    CP.updateConfig();
  }

  util.updateCtx();
});

document.addEventListener("mousemove", CP.mousemove.bind(CP));
document.addEventListener("mousemove", CP.mousemove2.bind(CP));
document.addEventListener("keypress", StrokeWidth.hotkey);


const canvasMousemoveFunction = {
  brush: Brush.draw,
  rectangle: Rectangle.draw,
  triangle: Triangle.draw.bind(Triangle),
  eraser: Brush.draw
};
const canvasMousedownFunction = {
  brush: (e) => { ctx.lineWidth = config.strokeWidth; },
  rectangle: (e) => { ctx.lineWidth = 5; },
  triangle: (e) => {
    ctx.lineWidth = 5;
    Triangle.calculatePathOffset();
  },
  eraser: () => { ctx.strokeStyle = init.canvasBackground; }
};

canvas.addEventListener("mousemove", (e) => {
  drawCursor.style.top = e.pageY - config.strokeWidth / 2;
  drawCursor.style.left = e.pageX - config.strokeWidth / 2;
  if(!status.isDrawing) return;
  // mousemove event is also fired with a mousedown event (if using touchpad, it's a double click)
  // so we have no need to implement draw function on mousedown event listener
  // but this feature seems to be a bug of...chrome..?
  // unfortunately, the next time when i tested, this not worked at all
  // I don't know why
  // tomokaku I add the mousemove function into mousedown event listener
  canvasMousemoveFunction[status.tool](e);
});
canvas.addEventListener("mousedown", (e) => {
  status.isDrawing = true;
  status.lastX = e.offsetX;
  status.lastY = e.offsetY;
  util.updateCtx();
  canvasMousedownFunction[status.tool](e);
  canvasMousemoveFunction[status.tool](e);
});
canvas.addEventListener("mouseenter", (e) => {
  status.lastX = e.offsetX;
  status.lastY = e.offsetY;
  drawCursor.style.display = "block";
});
canvas.addEventListener("mouseout", () => {
  drawCursor.style.display = "none";
});