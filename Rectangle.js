import {ctx, rectangle} from "./DOM.js";
import {config, util} from "./util.js";

const Rectangle = {
  draw: function(e){
    let [x, y] = [e.offsetX, e.offsetY];
    ctx.beginPath();
    ctx.moveTo(x - config.strokeWidth / 2, y - config.strokeWidth / 2);
    ctx.lineTo(x + config.strokeWidth / 2, y - config.strokeWidth / 2);
    ctx.lineTo(x + config.strokeWidth / 2, y + config.strokeWidth / 2);
    ctx.lineTo(x - config.strokeWidth / 2, y + config.strokeWidth / 2);
    ctx.fill();
    // for fun
    // ctx.stroke();
  },

  eventListenerRegister: function(){
    rectangle.btn.addEventListener("click", util.changeBrush.bind(null, "rectangle"));
  }
}

export {Rectangle};