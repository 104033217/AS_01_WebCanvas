import {colorPicker} from "./DOM.js";
import {status, config} from "./util.js";

const ColorPicker = {
  // H: 0~359
  // S: 0~100
  // L: 0~100
  H: 50,
  S: 50,
  L: 50,
  // 第一個panel 的mousedown function
  mousedown: function(e){
    // 更新color picker cursor 座標
    if(e.target === colorPicker.panel1){
      colorPicker.cursor1.style.top = e.offsetY - 5;
      colorPicker.cursor1.style.left = e.offsetX - 5;
    } else if (e.target === colorPicker.cursor1){
      // parseInt(): convert "??px" to "??"
      colorPicker.cursor1.style.top = parseInt(colorPicker.cursor1.style.top) + e.offsetY - 4;
      colorPicker.cursor1.style.left = parseInt(colorPicker.cursor1.style.left) + e.offsetX - 4;
    }
    // toggle status
    status.colorPicking1 = true;

    this.updateInfo();
  },
  // 第二個panel 的mousedown function
  mousedown2: function(e){
    // 更新color picker cursor 座標
    if(e.target === colorPicker.panel2){
      colorPicker.cursor2.style.left = e.offsetX;
    } else if (e.target === colorPicker.cursor2){
      colorPicker.cursor2.style.left = parseInt(colorPicker.cursor2.style.left) + e.offsetX;
    }
    // toggle status
    status.colorPicking2 = true;

    this.updateInfo();
  },

  mousemove: function(e){
    if(!status.colorPicking1) return;
    // 更新color picker cursor 座標
    colorPicker.cursor1.style.top = e.pageY - colorPicker.panel1.offsetTop - 6;
    colorPicker.cursor1.style.left = e.pageX - colorPicker.panel1.offsetLeft - 6;
    // 限制cursor 範圍
    // bug: by purly clicking on cursor itself, you can slowly move the cursor beyond the designed boundary
    if(e.pageY < colorPicker.panel1.offsetTop){  
      colorPicker.cursor1.style.top = -6;
    }
    if(e.pageX < colorPicker.panel1.offsetLeft){
      colorPicker.cursor1.style.left = -6;
    }
    if(e.pageY > colorPicker.panel1.offsetTop + colorPicker.panel1.offsetHeight){
      colorPicker.cursor1.style.top = colorPicker.panel1.offsetHeight - 6;
    }
    if(e.pageX > colorPicker.panel1.offsetLeft + colorPicker.panel1.offsetWidth){
      colorPicker.cursor1.style.left = colorPicker.panel1.offsetWidth - 6;
    }

    this.updateInfo();
  },
  mousemove2: function(e){
    if(!status.colorPicking2) return;
    // 更新color picker cursor 座標
    colorPicker.cursor2.style.left = e.pageX - colorPicker.panel2.offsetLeft;
    // 限制cursor 範圍
    // has the same bug with cursor1
    if(e.pageX < colorPicker.panel2.offsetLeft){
      colorPicker.cursor2.style.left = 0;
    }
    if(e.pageX > colorPicker.panel2.offsetLeft + colorPicker.panel2.offsetWidth){
      colorPicker.cursor2.style.left = colorPicker.panel2.offsetWidth;
    }

    this.updateInfo();
  },

  // 更新HSL色碼資訊
  updateInfo: function(){
    this.H = parseInt(colorPicker.cursor2.style.left);
    this.S = (parseInt(colorPicker.cursor1.style.left) + 6) / 2;
    this.L = 100 - (parseInt(colorPicker.cursor1.style.top) + 6) / 2;
    colorPicker.info[0].value = this.H;
    colorPicker.info[1].value = this.S;
    colorPicker.info[2].value = this.L;
    colorPicker.panel1.style.backgroundColor = `hsl(${this.H}, 100%, 50%)`;
    colorPicker.preview.style.backgroundColor = `hsl(${this.H}, ${this.S}%, ${this.L}%)`;
  },

  init: function(){
    colorPicker.info[0].value = this.H;
    colorPicker.cursor2.style.left = 50;
    colorPicker.info[1].value = this.S;
    colorPicker.info[2].value = this.L;
    colorPicker.cursor1.style.top = 50 - 6;
    colorPicker.cursor1.style.left = 50 - 6;
    colorPicker.panel1.style.backgroundColor = `hsl(${this.H}, 100%, 50%)`;
    colorPicker.preview.style.backgroundColor = `hsl(${this.H}, ${this.S}%, ${this.L}%)`;
    this.updateConfig();
  },

  updateConfig: () => {
    config.color = `hsl(${colorPicker.info[0].value}, ${colorPicker.info[1].value}%, ${colorPicker.info[2].value}%)`;
  },

  eventListenerRegister: function(){
    colorPicker.panel1.addEventListener("mousedown", this.mousedown.bind(this));
    colorPicker.panel2.addEventListener("mousedown", this.mousedown2.bind(this));
  }
};


export {ColorPicker};