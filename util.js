import {ctx, drawCursor, brush, rectangle, triangle, eraser} from "./DOM.js";

const util = {
  updateCtx: () => {
    ctx.lineWidth = config.strokeWidth;
    ctx.strokeStyle = config.color;
    ctx.fillStyle = config.color;
  },
  changeBrush: (tool) => {
    status.tool = tool;

    brush.btn.classList.remove("using");
    rectangle.btn.classList.remove("using");
    triangle.btn.classList.remove("using");
    eraser.btn.classList.remove("using");

    if(tool === "brush"){
      drawCursor.src = "./brush.svg";
      brush.btn.classList.add("using");
    } else if (tool === "rectangle"){
      drawCursor.src = "./rectangle.svg";
      rectangle.btn.classList.add("using");
    } else if (tool === "triangle"){
      drawCursor.src = "./triangle.svg";
      triangle.btn.classList.add("using");
    } else if (tool === "eraser"){
      drawCursor.src = "./brush.svg";
      eraser.btn.classList.add("using");
    }
  }
};

const init = {
  canvasBackground: "#eeeeee",
  canvasWidth: 800,
  canvasHeight: 600
};

const status = {
  isDrawing: false,
  colorPicking1: false,
  colorPicking2: false,
  strokeWidth: false,
  lastX: 0,
  lastY: 0,
  tool: "brush"
};

const config = {
  color: "#fff",
  strokeWidth: 10
};

const history = {
  f: [],
  g: [],
  susume: function(){
    this.f.push(ctx.getImageData(0, 0, init.canvasWidth, init.canvasHeight));
    this.g = [];
  }
};


export {util, init, status, config, history};