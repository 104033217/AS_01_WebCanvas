import {canvas, saveImage} from "./DOM.js";

const SaveImage = {
  saveImage: () => {
    let date = new Date();
    saveImage.link.download = `${date.getFullYear()}_${date.getMonth() + 1}_${date.getDate()}_${date.getHours()}${date.getMinutes()}${date.getSeconds()}`;
    saveImage.link.href = canvas.toDataURL();
    saveImage.link.click();
    // note: at initial or after reset canvas, canvas image data is empty with css background-color, so the download will get an empty png 
  },
  eventListenerRegister: function(){
    saveImage.btn.addEventListener("click", this.saveImage);
  }
}


export {SaveImage};