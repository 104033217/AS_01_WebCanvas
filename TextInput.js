import {textInput, ctx, canvas} from "./DOM.js";
import {config, history} from "./util.js";

const TextInput = {
  write: () => {
    textInput.ctx.clearRect(0, 0, textInput.canvas.width, textInput.canvas.height);
    textInput.ctx.fillStyle = config.color;
    textInput.ctx.font = `${textInput.fontSize.value}px ${textInput.font.value}`;
    textInput.ctx.fillText(textInput.input.value, 10, textInput.canvas.height - 10);
  },
  go: (e) => {
    // if no text, do nothing
    if(!textInput.input.value) return;
    
    // similar function as part of LoadImage.handleFile()

    // create new canvas
    let tmpCanvas = textInput.canvas.cloneNode(false);
    let tmpCtx = tmpCanvas.getContext("2d");
    tmpCtx.drawImage(textInput.canvas, 0, 0);
    tmpCanvas.style.cursor = "move";
    tmpCanvas.style.position = "absolute";
    tmpCanvas.style.left = textInput.canvas.offsetLeft;
    tmpCanvas.style.top = textInput.canvas.offsetTop;
    document.querySelector("body").appendChild(tmpCanvas);

    // logic of moving canvas
    let cursorOffsetLeft;
    let cursorOffsetTop;
    cursorOffsetTop = e.pageY - tmpCanvas.offsetTop;
    cursorOffsetLeft = e.pageX - tmpCanvas.offsetLeft;
    function moveCanvas(e){
      tmpCanvas.style.left = e.pageX - cursorOffsetLeft;
      tmpCanvas.style.top = e.pageY - cursorOffsetTop;
    }
    function place(){
      ctx.drawImage(
        tmpCanvas,
        parseInt(tmpCanvas.style.left) - canvas.offsetLeft,
        parseInt(tmpCanvas.style.top) - canvas.offsetTop,
        tmpCanvas.width,
        tmpCanvas.height
        );
      
      document.removeEventListener("mousemove", moveCanvas);
      tmpCanvas.remove();
      history.susume();
    }
    document.addEventListener("mousemove", moveCanvas);
    tmpCanvas.addEventListener("click", place);
  },
  eventListenerRegister: function(){
    textInput.input.addEventListener("keyup", this.write);
    textInput.fontSize.addEventListener("keyup", this.write);
    textInput.canvas.addEventListener("click", this.go);
  }
}


export {TextInput};