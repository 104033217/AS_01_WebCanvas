import {ctx, triangle} from "./DOM.js";
import {config, util} from "./util.js";

const Triangle = {
  pi: 3.141592653589793,
  offset_point2_x: -1,
  offset_point2_y: -1,
  offset_point3_x: -1,
  offset_point3_y: -1,

  draw: function(e){
    let [point1_x, point1_y] = [e.offsetX - config.strokeWidth / 2, e.offsetY + config.strokeWidth / 2];
    ctx.beginPath();
    // path:
    //     2
    //    /  \
    //   /    \
    //  1------3
    // don't forget the counterclockwise rotation by 20 degree
    ctx.moveTo(point1_x, point1_y);
    ctx.lineTo(point1_x + this.offset_point2_x , point1_y + this.offset_point2_y);
    ctx.lineTo(point1_x + this.offset_point3_x , point1_y + this.offset_point3_y);
    ctx.fill();
    // for fun
    // ctx.stroke();
  },

  calculatePathOffset(){
    this.offset_point2_x = config.strokeWidth * Math.tan(20/180*this.pi);
    this.offset_point2_y = - config.strokeWidth;
    // must be parsed or it will be treated as string
    this.offset_point3_x = parseInt(config.strokeWidth);
    this.offset_point3_y = - config.strokeWidth * Math.tan(10/180*this.pi);
  },

  eventListenerRegister: function(){
    triangle.btn.addEventListener("click", util.changeBrush.bind(null, "triangle"));
  }
}

export {Triangle};