
// common
export const canvas = document.querySelector(".canvas-main");
export const ctx = canvas.getContext("2d");
export const drawCursor = document.querySelector(".draw-cursor");
export const panel_left = document.querySelector(".panel-left");
export const panel_right = document.querySelector(".panel-right");

// StrokeWidth.js
export const strokeWidth = {
  input: panel_right.querySelector(".set-strokeWidth > input"),
  info: panel_right.querySelector(".set-strokeWidth > .info")
};

// ColorPicker.js
export const colorPicker = {
  panel1: panel_right.querySelector(".colorPicker-1"),
  panel2: panel_right.querySelector(".colorPicker-2"),
  cursor1: panel_right.querySelector(".colorPicker-1-cursor"),
  cursor2: panel_right.querySelector(".colorPicker-2-cursor"),
  info: panel_right.querySelectorAll(".colorPicker-data input[type=text]"),
  preview: panel_right.querySelector(".colorPicker-preview")
};

// ResetCanvas.js
export const resetCanvas = {
  btn: panel_right.querySelector(".clear-canvas > input")
}

// Undo.js
export const undo = {
  undoBtn: panel_left.querySelector(".set-tool input[value=undo]"),
  redoBtn: panel_left.querySelector(".set-tool input[value=redo]")
}

// Brush.js
export const brush = {
  btn: panel_left.querySelector(".set-tool input[value=brush]")
}

// LoadImage.js
export const loadImage = {
  btn: panel_right.querySelector(`input[value="load image"]`),
  template: document.querySelector(".load-image-template"),
  hidden: document.querySelector(".load-image-input")
}
export const container = document.querySelector(".container");

// Rectangle.js
export const rectangle = {
  btn: panel_left.querySelector(".set-tool input[value=rectangle]")
}

// Triangle.js
export const triangle = {
  btn: panel_left.querySelector(".set-tool input[value=triangle]")
}

// Eraser.js
export const eraser = {
  btn: panel_left.querySelector(".set-tool input[value=eraser]")
}

// SaveImage.js
export const saveImage = {
  btn: panel_right.querySelector(`input[value="save image"]`),
  link: document.querySelector("a#link")
}

// TextInput.js
const textTool = panel_right.querySelector(".tool-text");
export const textInput = {
  input: textTool.querySelector(".textInput"),
  fontSize: textTool.querySelector(".fontSize"),
  font: textTool.querySelector(".font"),
  canvas: textTool.querySelector("canvas"),
  ctx: textTool.querySelector("canvas").getContext("2d")
}