import {ctx, resetCanvas} from "./DOM.js";
import {init, history} from "./util.js";


const ResetCanvas = {
  reset: () => {
    ctx.clearRect(0, 0, init.canvasWidth, init.canvasHeight);
  },
  eventListenerRegister: function(){
    resetCanvas.btn.addEventListener("click", this.reset);
  }
}

export {ResetCanvas};