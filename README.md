# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here

<img src="./2019-03-28_213439.jpg">
<img src="./p1.png" width="600">

## demo
https://104033217.gitlab.io/AS_01_WebCanvas

## say something  

> 每個元件各自寫成object並獨立成一個 js 檔，在object內寫好回應滑鼠點擊、移動等events的method，最後再加上一個`eventListenerRegister()`  
> 由`main.js` import 並執行object內的`eventListenerRegister()` 註冊各個元件的eventListener  
>
> 畫布(canvas)和全頁面(document)的`addEventListener()`和相關functions 則寫在`main.js`裡面

* **DOM.js**  
  負責`querySelector()`取得頁面上的html element  
  其他 js 檔需要的elements由`DOM.js`import進來  

* **ColorPicker.js**  
  用HSL色碼  
  兩個ColorPicekr 面板:  
  - `panel1`(方形): 調整S(0~100), L(0~100)  
  - `panel2`(長條形): 調整H(0~360)  
   
  `mousedown()`, `mousedown2()`, `mousemove()`, `mousemove2()` 處理兩個panel的操作事件，詳細可看code註解、  
  `updateInfo()`更新顏色的預覽區塊和panel1的背景色，使用者可以直覺的用滑鼠操作兩個panel選色  
  `updateConfig()`實際更新畫筆的顏色設定  

* **StrokeWidth.js**  
  `hotkey()`快捷鍵， press [, ], shift + [, shift + ] 改變stroke width  

* **ResetCanvas.js**  
  `reset()`把畫布變透明哦~ (canvas API **`clearRect()`**)  

* **Undo.js**  
  `undo()`從過去的Array pop掉一個ImageData，把這個ImageData存到未來的Array，然後從過去Array的Top拿到ImageData重繪畫布(canvas API **`putImageData()`**)  
  `redo()`從未來Array pop掉一個ImageData，把這個ImageData拿來重繪畫布並push到過去Array  
  
* **Brush.js, Rectangle.js, Triangle.js**  
  這三個的`draw()`做的事情大同小異，都要利用canvas API Path2D(**`beginPath(), moveTo(), lineTo(), stroke(), fill()`**)  
  但是畫法有一點不同  
  - Brush(Circle) 直接畫線(<strong>`lineTo(), stroke()`</strong>)，線的粗細取決於程式中的`strokeWidth`值，然後因為在`main.js`裡面有初始化了<strong>`ctx.lineCap`</strong>，線的頭尾會是圓形的，這樣可以做到貼近繪圖軟體的畫筆功能  
  - Rectangle 沒有畫出線，呼叫<strong>`lineTo()`</strong>畫完邊緣之後呼叫<strong>`fill()`</strong>形成長方形，這樣子畫出來實際上是很多長方形疊起來的"線"。 非圓形的筆刷畫出的線寬取決於畫筆的移動方向，如果要做到像繪圖軟體一樣，每個線段都要先算出線寬才能呼叫<strong>`lineTo()`</strong>，因此會有很多額外的數學  
  - Triangle 跟Rectangle差不多，只是要多計算幾個角度  
  
* **util.js**  
  util, 類似`DOM.js`，存放其他元件可能會用到的variables & functions  
  - `init`: 畫布的初始設定，背景色、尺寸
  - `status`: 畫圖ing、選色ing、調整strokeWidth ing 的boolean、上一次游標位置(給`Brush.js`畫線用)、現在使用的tool
  - `config`: 畫筆顏色、粗細
  - `history`: 過去及未來的ImageData Array
  - `util`: 根據`config`更新筆刷的function、改變tool的function

* **LoadImage.js**  
  You may find the behavior of resizing the image uploaded is contrary to what you expect with the "proportional" option  
  there's an explain in `LoadImage.js`, please CtrlF "pitfall"  
    
  In briefly to the solution for user:  
  after you have loaded an image, you should 
  - first change the height of image(to what value is arbitrary)
  - second change it back
  - then change it's width, when doing this you can find the behavior will become conforming to the proportional option  

* **TextInput.js**  
  輸入的文字會即時顯示在下方預覽區，輸入完成後點一下預覽區  