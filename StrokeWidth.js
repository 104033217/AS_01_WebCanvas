import {status, config} from "./util.js"
import {drawCursor, strokeWidth} from "./DOM.js";

const StrokeWidth = {
  hotkey: (e) => {
    // 變化量
    let increment = e.shiftKey ? 6 : 1;
    // [ = 91
    // { = 123
    // ] = 93
    // } = 125
    if(e.keyCode === 91 || e.keyCode === 123){
      strokeWidth.input.value = Math.max(parseInt(strokeWidth.input.value) - increment, 1);
      // dispatch an event by programming
      let event = new Event("change");
      strokeWidth.input.dispatchEvent(event);
    } else if(e.keyCode === 93 || e.keyCode === 125){
      strokeWidth.input.value = Math.min(parseInt(strokeWidth.input.value) + increment, 150);
      let event = new Event("change");
      strokeWidth.input.dispatchEvent(event);
    }
  },
  eventListenerRegister: function(){
    strokeWidth.input.addEventListener("mouseup", (e) => {
      status.strokeWidth = false;
    });
    strokeWidth.input.addEventListener("mousedown", (e) => {
      status.strokeWidth = true;
    });
    strokeWidth.input.addEventListener("mousemove", (e) => {
      if(status.strokeWidth){
        strokeWidth.info.textContent = e.target.value;
      }
    });
    // arrow function doesn't have "this", so here "this" refers to StrokeWidth without .bind()
    // (I guess)
    strokeWidth.input.addEventListener("change", (e) => {
      config.strokeWidth = e.target.value;
      strokeWidth.info.textContent = e.target.value;
      this.updateCursorSvg();
    });
  },
  // update cursor img element's width & height
  updateCursorSvg(){
    drawCursor.width = Math.max(config.strokeWidth, 5);
    drawCursor.height = Math.max(config.strokeWidth, 5);
  }
}

export {StrokeWidth};