import {undo, ctx} from "./DOM.js";
import {history} from "./util.js";
import {ResetCanvas} from "./ResetCanvas.js";

const Undo = {
  undo: () => {
    if(history.f.length === 0) return;
    history.g.push( history.f.pop() );
    if(history.f.length === 0){
      ResetCanvas.reset();
    } else {
      ctx.putImageData(history.f[history.f.length - 1], 0, 0);
    }
  },
  redo: () => {
    let imagedata = history.g.pop();
    if(imagedata){
      history.f.push(imagedata);
      ctx.putImageData(imagedata, 0, 0);
    }
    // console.log(history.f, history.g);
  },

  eventListenerRegister: function(){
    undo.undoBtn.addEventListener("click", this.undo);
    undo.redoBtn.addEventListener("click", this.redo);
  }
}

export {Undo}